// add to the javascript array the command to insert the positions

let res = [
    db.bankAccounts.insert(
        {
             "userName": "aiman",
             "amount": 100.0,
             "accountHistory":[
                 {
                     "amount": 100,
                     "fromName": "aiman",
                     "positions": []
                 },
                 {
                     "amount": -100,
                     "positions":[],
                     "fromName": "aiman"
                 }
             ]
        }),
    db.bankAccounts.insert(
        {
            "userName": "alessandro",
            "amount": 200.0,
            "accountHistory":[
                {
                    "amount":200,
                    "positions":[],
                    "fromName": "alessandro"
                },
                {
                    "amount":-50,
                    "positions":[],
                    "fromName": "alessandro"
                }
            ]
        }),
        db.bankAccounts.insert(
	{
            "userName": "enrico",
            "amount": 200.0,
            "accountHistory":[
                {
                    "amount":150,
                    "positions":[],
                    "fromName": "enrico"
                },
                {
                    "amount":50,
                    "positions":[],
                    "fromName": "alessandro"
                }
            ]
        }
    ),
    db.positions.insert(
        {
            "timestamp": 1528374839,
            "cost": 0.5,
            "userName": "enrico",
            "geoData": {
                "type": "Point",
                "coordinates": [
                        7.655212,
			45.053351
                ]
            }
        }),

    db.positions.insert(
        {
            "timestamp": 1528374839,
            "userName": "enrico",
            "cost": 0.5,
            "geoData": {
                "type": "Point",
                "coordinates": [                    
		    7.655244,
                    45.051805
                ]
            }
        }),

    db.positions.insert(
        {
            "timestamp": 1528374839,
            "cost": 0.5,
            "userName": "enrico",
            "geoData": {
                "type": "Point",
                "coordinates": [                    7.656506,
                    45.051066
                ]
            }
        }),

    db.positions.insert(
        {
            "timestamp": 1528374839,
            "userName": "enrico",
            "cost": 0.5,
            "geoData": {
                "type": "Point",
                "coordinates": [                    7.658834,
                    45.050573
                ]
            }
        }),

    db.positions.insert(
        {
            "timestamp": 1528374839,
            "cost": 0.5,
            "userName": "enrico",
            "geoData": {
                "type": "Point",
                "coordinates": [                    7.660483,
                    45.050487
                ]
            }
        })
]

printjson(res);
