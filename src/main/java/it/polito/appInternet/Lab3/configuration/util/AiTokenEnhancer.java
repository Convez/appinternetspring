package it.polito.appInternet.Lab3.configuration.util;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

/**
 * Used by the Authorization Server to put custom claims within the JWT token
 */
public class AiTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        // Here an example about how to add some custom claims into the access token
        /*
        User user = (User)authentication.getPrincipal();
        Map<String, Object> additionalInfo = new HashMap<>();
        for (Authority authority : user.getAuthorities()) {
            additionalInfo.put("ROLES", authority.getAuthority());
        }
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo); */
        return accessToken;
    }
}
