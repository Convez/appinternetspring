package it.polito.appInternet.Lab3.configuration.mongodb;

import com.mongodb.WriteConcern;
import org.springframework.data.mongodb.core.MongoAction;

public class WriteConcernImpl implements WriteConcernResolver {

    @Override
    public WriteConcern resolve(MongoAction action) {

        return WriteConcern.ACKNOWLEDGED;
    }
}
