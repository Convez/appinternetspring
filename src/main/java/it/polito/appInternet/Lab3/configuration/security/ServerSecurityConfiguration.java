package it.polito.appInternet.Lab3.configuration.security;

import it.polito.appInternet.Lab3.configuration.util.Encoders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
// ACCESS_OVERRIDE_ORDER deleted. As pointed out by the documentation:
// the best thing to do is to add your own WebSecurityConfigurerAdapter with lower order.
// https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/security/SecurityProperties.html
@Order(SecurityProperties.BASIC_AUTH_ORDER - 1)
@Import(Encoders.class)
public class ServerSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("AiUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder userPasswordEncoder;

    @Bean
    public FilterRegistrationBean corsConfigurationSource(){
        CorsConfiguration config = new CorsConfiguration();
        config.applyPermitDefaultValues();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList("*"));
        config.setAllowedHeaders(Arrays.asList("*"));
        config.setAllowedMethods(Arrays.asList("*"));
        config.setExposedHeaders(Arrays.asList("content-length"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/oauth/token", config);
        source.registerCorsConfiguration("/secured/**",config);
        source.registerCorsConfiguration("/signUp", config);
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(new CorsFilter(source));
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registrationBean;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {

        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsService).passwordEncoder(userPasswordEncoder);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        // ToDo Remove csrf disable
        http.csrf().disable().authorizeRequests().antMatchers("/login*")
                .anonymous().antMatchers("/css/*").anonymous().antMatchers("/oauth_access")
                .anonymous().antMatchers("/signUp").permitAll().anyRequest().authenticated()
/*                .and().oauth2Login()
                .authorizationEndpoint().baseUri("/oauth_authorize").authorizationRequestRepository(authorizationRequestRepository())
                .and().redirectionEndpoint().baseUri("/login/oauth2/callback/*")
                .and().loginPage("/oauth_access")*/.and().formLogin().loginPage("/login.html").loginProcessingUrl("/do_login");
    }

    @Bean
    public AuthorizationRequestRepository<OAuth2AuthorizationRequest> authorizationRequestRepository() {

        return new HttpSessionOAuth2AuthorizationRequestRepository();
    }

}
