package it.polito.appInternet.Lab3.configuration.mongodb;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonLineString;

import java.io.IOException;

public class GeoJsonLineStringSerializer extends JsonSerializer<GeoJsonLineString> {

    /*
        used the code from this url:
        https://www.alessandrorosa.com/geojson-serialization-issues-with-spring-data-mongodb/
     */
    @Override
    public void serialize(GeoJsonLineString value, JsonGenerator gen, SerializerProvider serializers) throws IOException {

        gen.writeStartObject();
        gen.writeStringField("type", value.getType());
        gen.writeArrayFieldStart("coordinates");
        for (Point p : value.getCoordinates()) {
            gen.writeObject(new double[]{p.getX(), p.getY()});
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }
}
