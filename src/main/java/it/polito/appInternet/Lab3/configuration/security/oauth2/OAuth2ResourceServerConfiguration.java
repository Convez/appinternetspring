package it.polito.appInternet.Lab3.configuration.security.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@Configuration
@EnableResourceServer
@PropertySource({"classpath:resource_server_config.properties"})
public class OAuth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    private final Environment env;
    //private final CustomAccessTokenConverter customAccessTokenConverter;
    private final DefaultTokenServices tokenServices;

    @Autowired
    public OAuth2ResourceServerConfiguration(Environment env, DefaultTokenServices tokenServices) {

        this.env = env;
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {

        resources.resourceId(env.getProperty("resource_id"));
        resources.tokenServices(tokenServices);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        // ToDo To review based on our criteria
        http.csrf().disable().requestMatchers().antMatchers(env.getProperty("secured_pattern")).and().authorizeRequests().anyRequest().access(SECURED_READ_SCOPE);
    }

    // IMPLEMENTATION TO BE USED IF THE RESOURCE SERVER IS IN A COMPLETELY INDEPENDENT SERVER WITH RESPECT TO THE
    // AUTHORIZATION SERVER
    /*
        @Bean
    public JwtAccessTokenConverter resourceAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setAccessTokenConverter(customAccessTokenConverter);

        Resource resource = new ClassPathResource("security/public.txt");
        String publicKey;
        Charset charset = null;
        try {
            publicKey = IOUtils.toString(resource.getInputStream(), charset);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        converter.setVerifierKey(publicKey);
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(resourceAccessTokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }
     */
}
