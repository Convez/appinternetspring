package it.polito.appInternet.Lab3.model.bank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "Transactions")
public class BankTransaction {

    @Id
    private ObjectId id;

    private String stringId;

    private Double amount;

    private String fromName;

    @Indexed
    @JsonIgnore
    private String userName;

    private Map<String, String> archiveIds;

    public Map<String, String> getArchiveIds() {
        return archiveIds;
    }

    public void setArchiveIds(Map<String, String> archiveIds) {
        this.archiveIds = archiveIds;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public String getFromName() {

        return fromName;
    }

    public void setFromName(String beneficiaryName) {
        this.fromName = beneficiaryName;
    }
}
