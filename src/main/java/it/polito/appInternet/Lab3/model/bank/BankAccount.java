package it.polito.appInternet.Lab3.model.bank;


import org.bson.types.ObjectId;
import org.hibernate.annotations.Index;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;

@Document(collection = "bankAccounts")
public class BankAccount {

    @Id
    private ObjectId id;

    @Indexed
    private String userName;

    private Double amount;

    private Collection<String> accountHistory;

    public void setAccountHistory(Collection<String> accountHistory) {
        this.accountHistory = accountHistory;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Collection<String> getAccountHistory() {
        return accountHistory;
    }

    public void addTransaction(String transaction) {
        this.accountHistory.add(transaction);
    }
}
