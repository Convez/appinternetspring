package it.polito.appInternet.Lab3.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.polito.appInternet.Lab3.serialization.GeoJsonPolygonSerializer;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;

public class PolygonSearchRequest {

    @JsonSerialize(using = GeoJsonPolygonSerializer.class)
    @JsonProperty("innerPolygon")
    private GeoJsonPolygon innerPolygon;
    @JsonProperty("before")
    private Long before;
    @JsonProperty("after")
    private Long after;
    public PolygonSearchRequest(){

    }
    public GeoJsonPolygon getInnerPolygon() {return innerPolygon;}
    public void setInnerPolygon(GeoJsonPolygon geoJsonPolygon){
        innerPolygon =geoJsonPolygon;
    }

    public Long getBefore() {
        return before;
    }

    public void setBefore(Long before) {
        this.before = before;
    }

    public Long getAfter() {
        return after;
    }

    public void setAfter(Long after) {
        this.after = after;
    }
}
