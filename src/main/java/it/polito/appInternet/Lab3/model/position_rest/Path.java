package it.polito.appInternet.Lab3.model.position_rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Null;

@Document(collection = "path")
public class Path extends ResourceSupport {

    @Version
    @JsonIgnore
    @Transient
    Long version;
    @Valid Positions positions;
    @Id
    @Null
    private String pathId;
    @Indexed
    @Null
    private String userId;
    @Min(value = 0)
    private long beginDate;
    @Min(value = 0)
    private long endDate;

    public String getPathId() {

        return pathId;
    }

    public void setPathId(String pathId) {

        this.pathId = pathId;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public long getBeginDate() {

        return beginDate;
    }

    public void setBeginDate(long beginDate) {

        this.beginDate = beginDate;
    }

    public long getEndDate() {

        return endDate;
    }

    public void setEndDate(long endDate) {

        this.endDate = endDate;
    }

    public Long getVersion() {

        return version;
    }

    public void setVersion(Long version) {

        this.version = version;
    }

    public Positions getPositions() {

        return positions;
    }

    public void setPositions(Positions positions) {

        this.positions = positions;
    }
}
