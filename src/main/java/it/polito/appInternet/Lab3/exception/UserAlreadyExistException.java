package it.polito.appInternet.Lab3.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class UserAlreadyExistException extends RuntimeException {

        public UserAlreadyExistException() {
        }

        public UserAlreadyExistException(String message) {
            super(message);
        }

}
