package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.position_rest.Path;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PathRepository extends MongoRepository<Path, String> {

    List<Path> findByUserId(String userID);

    Optional<Path> findByPathId(String pathId);

    long deleteByPathIdAndUserId(String pathId, String userId);

    List<Path> findByBeginDateGreaterThanEqual(long date);

    List<Path> findByEndDateLessThanEqual(long date);
}
