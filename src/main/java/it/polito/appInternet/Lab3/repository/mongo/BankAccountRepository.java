package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.bank.BankAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BankAccountRepository extends MongoRepository<BankAccount,String> {

    BankAccount findBankAccountByUserName(String userName);
}
