package it.polito.appInternet.Lab3.repository.jpa;

import it.polito.appInternet.Lab3.model.security.Authority;
import it.polito.appInternet.Lab3.model.security.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@Repository
public interface JPAUserRepository extends JpaRepository<User, Long> {

    @Query("SELECT DISTINCT user FROM User user " + "INNER JOIN FETCH user.authorities AS authorities " + "WHERE user.username = :username")
    User findByUsername(@Param("username") String username);

    @Query("SELECT DISTINCT user FROM User user " + "INNER JOIN FETCH user.authorities AS authorities ")
    List<User> getAllUsers();

}
