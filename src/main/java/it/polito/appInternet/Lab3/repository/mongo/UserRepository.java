package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.position_rest.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUsername(String username);
}
