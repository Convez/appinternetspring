package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.PositionsArchive;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface PositionsArchiveRepository extends MongoRepository<PositionsArchive,String> {
    Optional<PositionsArchive> findPositionsArchiveById(ObjectId id);
    Stream<PositionsArchive> findPositionsArchivesByIdIn(List<ObjectId> objectIds);
    Stream<PositionsArchive> findPositionsArchivesByIdIn(Set<String> objectIds);
    Stream<PositionsArchive> findPositionsArchivesByUserName(String userName);
    Stream<PositionsArchive> findPositionsArchivesByUserNameAndAndSearchable(String userName, Boolean searchable);
    Stream<PositionsArchive> findPositionsArchivesByApproxPositionsIsWithin(GeoJsonPolygon polygon);
    List<PositionsArchive> findPositionsArchiveByIdAndUserName(ObjectId archiveId, String username);
    Long deletePositionsArchiveByIdAndUserName(ObjectId archiveId, String username);
}
