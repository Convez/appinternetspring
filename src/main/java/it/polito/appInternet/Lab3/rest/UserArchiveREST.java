package it.polito.appInternet.Lab3.rest;

import it.polito.appInternet.Lab3.configuration.util.JwtUtils;
import it.polito.appInternet.Lab3.exception.AlreadyUploadedException;
import it.polito.appInternet.Lab3.model.GetAllResponse;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.service.ArchivePersistenceService;
import it.polito.appInternet.Lab3.service.PositionValidatorService;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@CrossOrigin
@RestController
@RequestMapping("/secured/user/")
public class UserArchiveREST {

    private final ArchivePersistenceService archivePersistenceService;
    private final PositionValidatorService positionValidatorService;
    private final JwtUtils jwtUtils;

    public UserArchiveREST(ArchivePersistenceService archivePersistenceService, PositionValidatorService positionValidatorService, JwtUtils jwtUtils) {
        this.archivePersistenceService = archivePersistenceService;
        this.positionValidatorService = positionValidatorService;
        this.jwtUtils = jwtUtils;
    }
    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public GetAllResponse getAll(
            @RequestHeader(value="Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);

        return new GetAllResponse(
            archivePersistenceService.retrieveFilledUploadedArchives(userName).collect(Collectors.toList()),
            archivePersistenceService.retrieveFilledBoughtArchives(userName).collect(Collectors.toList()));
    }
    @RequestMapping(
            value = "/archive",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public void uploadArchive(
            @Valid @RequestBody @NotNull List<UserPosition> positions,
            BindingResult result,
            @RequestHeader(value="Authorization") String accessToken
    ){
        //Check if the list of positions is a valid list
        if(result.hasErrors())
            throw new ValidationException();
        if(!positionValidatorService.validatePositionList(positions))
            throw new ValidationException();

        //Set username
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        for(UserPosition p : positions){
            p.setUserName(userName);
            p.setCost(0.5);
            p.setSearchable(true);
        }

        if(archivePersistenceService.isAlreadyUploaded(userName,positions))
            throw new AlreadyUploadedException();

        archivePersistenceService.createAndSaveArchiveFrom(userName,positions);
    }

    @RequestMapping(
            value = "/archive",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public Stream<PositionsArchive> getArchives (
            @RequestHeader(value = "Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return archivePersistenceService.retrieveUserUploadedArchives(userName);
    }

    @RequestMapping(
            value = "/archive/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public Stream<UserPosition> getArchivePositions (
            @PathVariable("id") ObjectId archiveId,
            @RequestHeader(value = "Authorization") String accessToken
    ){
        System.out.println(archiveId);
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return archivePersistenceService.retrieveArchiveDetails(userName, archiveId);
    }

    @RequestMapping(
            value = "/archive/{id}/visibility",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public void hideArchiveFromSearch (
            @PathVariable("id") ObjectId archiveId,
            @RequestParam("visibility") Boolean visibility,
            @RequestHeader(value = "Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        archivePersistenceService.setArchiveSearchVisibility(userName,archiveId,visibility);
    }
}
