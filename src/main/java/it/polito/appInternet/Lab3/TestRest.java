package it.polito.appInternet.Lab3;

import it.polito.appInternet.Lab3.configuration.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/secured/test") 
public class TestRest {

    @Autowired
    JwtUtils jwtUtils;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ADMIN', 'USER', 'CUSTOMER')")
    public @ResponseBody
    String helloWorld(@RequestHeader(value = "Authorization") String accessToken) {

        return "Hello " + jwtUtils.getUsernameFromToken(accessToken) + "!";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    String helloAdmin(@RequestHeader(value = "Authorization") String accessToken) {

        return "Hello admin " + jwtUtils.getUsernameFromToken(accessToken) + "!";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')")
    public @ResponseBody
    String helloUser(@RequestHeader(value = "Authorization") String accessToken) {

        return "Hello user " + jwtUtils.getUsernameFromToken(accessToken) + "!";
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
@PreAuthorize("hasRole('USER')")
    public @ResponseBody
    String helloCustomer(@RequestHeader(value = "Authorization") String accessToken) {

        return "Hello customer " + jwtUtils.getUsernameFromToken(accessToken) + "!";
    }
}
