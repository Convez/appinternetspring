package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.model.bank.BankTransaction;
import org.bson.types.ObjectId;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface BankAccountService {
    Collection<BankTransaction> getHistory(String userName);

    Map<String,Boolean> checkArchivesAreOwned(String userName, List<ObjectId> archivesIds);

    @Transactional
    Stream<PositionsArchive> buyArchives(String userName, Collection<PositionsArchive> archives);

    Stream<UserPosition> getTransactionArchivePositions(String userName, ObjectId transactionId, ObjectId archiveId);
}
