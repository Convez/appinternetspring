package it.polito.appInternet.Lab3.service;


import it.polito.appInternet.Lab3.model.UserPosition;

import java.util.List;

public interface PositionValidatorService {

    boolean validatePositionList(List<UserPosition> positions);
    List<UserPosition> validateAndMergePosition(List<UserPosition> DbPositions, List<UserPosition> requestPositions);
}
