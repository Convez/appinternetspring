package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.PolygonSearchRequest;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

public interface CustomerPositionService {
    Stream<PositionsArchive> getPositionInsidePolygon(String userName, PolygonSearchRequest polygonSearchRequest);
}
