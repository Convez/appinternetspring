package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.security.UserDto;

public interface SignUpService {
    void registerNewUserAccount(UserDto userDto);

    boolean userExist(String username);
}
