package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.security.User;
import it.polito.appInternet.Lab3.repository.jpa.JPAUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Component("AiUserDetailsService")
public class AiUserDetailsService implements UserDetailsService {

    private final JPAUserRepository JPAUserRepository;

    @Autowired()
    public AiUserDetailsService(@Qualifier("JPAUserRepository") JPAUserRepository JPAUserRepository) {

        this.JPAUserRepository = JPAUserRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = JPAUserRepository.findByUsername(username);
        if (user != null) {
            return user;
        }

        throw new UsernameNotFoundException(username);
    }

    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public boolean create(User user) {

        if (!exist(user.getUsername())) {
            JPAUserRepository.save(user);
            return true;
        } else return false;
    }

    private boolean exist(String username) {

        try {
            loadUserByUsername(username);
            return true;
        } catch (UsernameNotFoundException e) {
            return false;
        }
    }

    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(Long id) {

        JPAUserRepository.deleteById(id);
    }

    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(User user) {

        JPAUserRepository.delete(user);
    }
}
