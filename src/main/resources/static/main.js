(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-drawer-container class=\"sidenav-container\">\n  <mat-drawer mode=\"side\" [mode]=\"over\" #sideMenu closed (close)=\"onSidenavClose()\">\n      <lab4-sidebar></lab4-sidebar>\n    </mat-drawer>\n      <mat-drawer-content>\n      <mat-toolbar class=\"mat-top-toolbar\" color=\"primary\">\n        <button mat-icon-button id=\"menuButton\" #menuButton (click)=\"menuButtonClick()\">\n          <mat-icon>list</mat-icon>\n        </button>\n      </mat-toolbar>\n      <router-outlet></router-outlet>\n    </mat-drawer-content>\n  </mat-drawer-container>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  width: 100%;\n  height: 100%;\n  margin: 0px; }\n\n.mat-top-toolbar {\n  height: 7%; }\n\n@media screen and (min-width: 992px) {\n  #menuButton {\n    visibility: hidden; } }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        var _this = this;
        this.router = router;
        this.onSidenavClose = function () {
            _this.menuButton._elementRef.nativeElement.classList.remove('cdk-program-focused');
            _this.menuButton._elementRef.nativeElement.classList.remove('cdk-mouse-focused');
        };
        this.menuButtonClick = function () {
            _this.sidenav.toggle();
        };
        this.title = 'app';
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events
            .subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (_this.sidenav.opened)
                    _this.sidenav.toggle();
                _this.menuButton._elementRef.nativeElement.classList.remove('cdk-program-focused');
                _this.menuButton._elementRef.nativeElement.classList.remove('cdk-mouse-focused');
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("sideMenu"),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDrawer"])
    ], AppComponent.prototype, "sidenav", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("menuButton"),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
    ], AppComponent.prototype, "menuButton", void 0);
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _lab4_sidebar_component_lab4_sidebar_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./lab4.sidebar.component/lab4.sidebar.module */ "./src/app/lab4.sidebar.component/lab4.sidebar.module.ts");
/* harmony import */ var _lab4_login_component_lab4_login_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lab4.login.component/lab4.login.module */ "./src/app/lab4.login.component/lab4.login.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.routing.module */ "./src/app/app.routing.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _lab4_serverRequests_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./lab4.serverRequests.service */ "./src/app/lab4.serverRequests.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _lab4_notfound_component_lab4_notfound_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./lab4.notfound.component/lab4.notfound.module */ "./src/app/lab4.notfound.component/lab4.notfound.module.ts");
/* harmony import */ var _lab4_register_component_lab4_register_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./lab4.register.component/lab4.register.module */ "./src/app/lab4.register.component/lab4.register.module.ts");
/* harmony import */ var _lab4_positionSearch_component_lab4_positionSearch_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./lab4.positionSearch.component/lab4.positionSearch.module */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.module.ts");
/* harmony import */ var _lab5_authenticationService_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./lab5.authenticationService.service */ "./src/app/lab5.authenticationService.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"],
                _lab4_sidebar_component_lab4_sidebar_module__WEBPACK_IMPORTED_MODULE_5__["Lab4SidebarModule"],
                _lab4_login_component_lab4_login_module__WEBPACK_IMPORTED_MODULE_6__["Lab4LoginModule"],
                _lab4_notfound_component_lab4_notfound_module__WEBPACK_IMPORTED_MODULE_12__["Lab4NotFoundModule"],
                _lab4_register_component_lab4_register_module__WEBPACK_IMPORTED_MODULE_13__["Lab4RegisterModule"],
                _lab4_positionSearch_component_lab4_positionSearch_module__WEBPACK_IMPORTED_MODULE_14__["Lab4PositionSearchModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"]
            ],
            providers: [
                _lab4_serverRequests_service__WEBPACK_IMPORTED_MODULE_10__["Lab4ServerRequestsService"],
                _lab5_authenticationService_service__WEBPACK_IMPORTED_MODULE_15__["Lab5AuthenticationService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app.routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lab4_login_component_lab4_login_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lab4.login.component/lab4.login.module */ "./src/app/lab4.login.component/lab4.login.module.ts");
/* harmony import */ var _lab4_login_component_lab4_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lab4.login.component/lab4.login.component */ "./src/app/lab4.login.component/lab4.login.component.ts");
/* harmony import */ var _lab4_notfound_component_lab4_notfound_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./lab4.notfound.component/lab4.notfound.component */ "./src/app/lab4.notfound.component/lab4.notfound.component.ts");
/* harmony import */ var _lab4_notfound_component_lab4_notfound_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./lab4.notfound.component/lab4.notfound.module */ "./src/app/lab4.notfound.component/lab4.notfound.module.ts");
/* harmony import */ var _lab4_register_component_lab4_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lab4.register.component/lab4.register.component */ "./src/app/lab4.register.component/lab4.register.component.ts");
/* harmony import */ var _lab4_register_component_lab4_register_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lab4.register.component/lab4.register.module */ "./src/app/lab4.register.component/lab4.register.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _lab4_positionSearch_component_lab4_positionSearch_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./lab4.positionSearch.component/lab4.positionSearch.module */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.module.ts");
/* harmony import */ var _lab4_positionSearch_component_lab4_positionSearch_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./lab4.positionSearch.component/lab4.positionSearch.component */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _lab4_login_component_lab4_login_module__WEBPACK_IMPORTED_MODULE_2__["Lab4LoginModule"],
                _lab4_positionSearch_component_lab4_positionSearch_module__WEBPACK_IMPORTED_MODULE_9__["Lab4PositionSearchModule"],
                _lab4_notfound_component_lab4_notfound_module__WEBPACK_IMPORTED_MODULE_5__["Lab4NotFoundModule"],
                _lab4_register_component_lab4_register_module__WEBPACK_IMPORTED_MODULE_7__["Lab4RegisterModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot([
                    { path: 'home', component: _lab4_positionSearch_component_lab4_positionSearch_component__WEBPACK_IMPORTED_MODULE_10__["Lab4PositionSearch"] },
                    { path: 'login', component: _lab4_login_component_lab4_login_component__WEBPACK_IMPORTED_MODULE_3__["Lab4LoginComponent"] },
                    { path: 'register', component: _lab4_register_component_lab4_register_component__WEBPACK_IMPORTED_MODULE_6__["Lab4RegisterComponent"] },
                    { path: "", redirectTo: "home", pathMatch: "full" },
                    { path: '**', component: _lab4_notfound_component_lab4_notfound_component__WEBPACK_IMPORTED_MODULE_4__["Lab4NotFoundComponent"] },
                ])
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"],
            ],
            providers: [],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/lab4.login.component/lab4.login.component.html":
/*!****************************************************************!*\
  !*** ./src/app/lab4.login.component/lab4.login.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-card class=\"login-card\" fxFlex>\n        <mat-card-title-group>\n            <mat-card-title class=\"login-header\">\n                {{headerText}}\n            </mat-card-title>\n        </mat-card-title-group>\n        <mat-card-content class=\"form-content\">\n        \n        <mat-form-field>\n            <mat-icon matPrefix>person</mat-icon>\n            <input matInput [(ngModel)]=\"username\" placeholder=\"Username\">\n        </mat-form-field>\n        <mat-form-field>\n            <mat-icon matPrefix>lock</mat-icon>\n            <input matInput [(ngModel)]=\"password\" placeholder=\"Password\" type=\"password\">\n        </mat-form-field>\n        </mat-card-content>\n        <mat-card-actions>\n            <button color=\"primary\" mat-button routerLinkActive=\"active\" \n            routerLink=\"/register\">Register</button>\n            <button color=\"primary\" (click)=\"doLogin()\" mat-raised-button>Login</button>\n        </mat-card-actions>\n</mat-card>"

/***/ }),

/***/ "./src/app/lab4.login.component/lab4.login.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/lab4.login.component/lab4.login.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-card {\n  max-width: 500px;\n  margin: 0 auto;\n  margin-top: 15px; }\n\n.form-content {\n  display: flex;\n  flex-direction: column; }\n\nmat-form-field {\n  margin-top: 8px; }\n\nmat-icon {\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/lab4.login.component/lab4.login.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/lab4.login.component/lab4.login.component.ts ***!
  \**************************************************************/
/*! exports provided: Lab4LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4LoginComponent", function() { return Lab4LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lab5_authenticationService_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../lab5.authenticationService.service */ "./src/app/lab5.authenticationService.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Lab4LoginComponent = /** @class */ (function () {
    function Lab4LoginComponent(authServ) {
        var _this = this;
        this.authServ = authServ;
        this.headerText = "Log In";
        this.doLogin = function () {
            console.log(_this.username);
            console.log(_this.password);
            _this.authServ.login(_this.username, _this.password);
        };
    }
    Lab4LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-login',
            template: __webpack_require__(/*! ./lab4.login.component.html */ "./src/app/lab4.login.component/lab4.login.component.html"),
            styles: [__webpack_require__(/*! ./lab4.login.component.scss */ "./src/app/lab4.login.component/lab4.login.component.scss")]
        }),
        __metadata("design:paramtypes", [_lab5_authenticationService_service__WEBPACK_IMPORTED_MODULE_1__["Lab5AuthenticationService"]])
    ], Lab4LoginComponent);
    return Lab4LoginComponent;
}());



/***/ }),

/***/ "./src/app/lab4.login.component/lab4.login.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/lab4.login.component/lab4.login.module.ts ***!
  \***********************************************************/
/*! exports provided: Lab4LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4LoginModule", function() { return Lab4LoginModule; });
/* harmony import */ var _lab4_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lab4.login.component */ "./src/app/lab4.login.component/lab4.login.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var Lab4LoginModule = /** @class */ (function () {
    function Lab4LoginModule() {
    }
    Lab4LoginModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _lab4_login_component__WEBPACK_IMPORTED_MODULE_0__["Lab4LoginComponent"]
            ],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]
            ],
            providers: [],
            entryComponents: [],
            exports: [_lab4_login_component__WEBPACK_IMPORTED_MODULE_0__["Lab4LoginComponent"]]
        })
    ], Lab4LoginModule);
    return Lab4LoginModule;
}());



/***/ }),

/***/ "./src/app/lab4.map.component/lab4.map.component.html":
/*!************************************************************!*\
  !*** ./src/app/lab4.map.component/lab4.map.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #map class=\"map\"\nleaflet\n[leafletOptions]=\"options\"\n(leafletMapReady)=\"mapReadyCallback($event)\"\n>\n    <div\n        class=\"mapDraw\"\n        #mapDraw\n        leafletDraw\n        [leafletDrawOptions]=\"drawOptions\"\n    >\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/lab4.map.component/lab4.map.component.scss":
/*!************************************************************!*\
  !*** ./src/app/lab4.map.component/lab4.map.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\");\n.map {\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/lab4.map.component/lab4.map.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/lab4.map.component/lab4.map.component.ts ***!
  \**********************************************************/
/*! exports provided: Lab4MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4MapComponent", function() { return Lab4MapComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var jqueryui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jqueryui */ "./node_modules/jqueryui/jquery-ui.js");
/* harmony import */ var jqueryui__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jqueryui__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _lab4_positionSearch_component_lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../lab4.positionSearch.component/lab4.positionSearch.service */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.service.ts");
/* harmony import */ var _lab4_polygonInput_component_lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../lab4.polygonInput.component/lab4.polygonInput.component */ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Lab4MapComponent = /** @class */ (function () {
    function Lab4MapComponent(positionSearchService, dialog) {
        var _this = this;
        this.positionSearchService = positionSearchService;
        this.dialog = dialog;
        this.alreadySet = false;
        this.drawOptions = {
            position: 'topright',
            draw: {
                polyline: false,
                circle: false,
                marker: false,
                circlemarker: false
            }
        };
        this.options = {
            layers: [
                leaflet__WEBPACK_IMPORTED_MODULE_1__["tileLayer"]('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                    maxZoom: 20,
                    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                    detectRetina: true
                }),
            ],
            bounds: leaflet__WEBPACK_IMPORTED_MODULE_1__["latLngBounds"](leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"](0, -180), leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"](85, 0)),
            zoom: 7,
            center: leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"]([46.879966, -121.726909])
        };
        this.mapReadyCallback = function (map) {
            _this.map = map;
            var southWest = leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"](0, -180), northEast = leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"](85, 0), bounds = leaflet__WEBPACK_IMPORTED_MODULE_1__["latLngBounds"](southWest, northEast);
            _this.map.fitBounds(bounds);
            map.on(leaflet__WEBPACK_IMPORTED_MODULE_1__["Draw"].Event.CREATED, _this.polygonDrawedCallback);
            //Start location
            var options = {
                setView: false,
                maxZoom: 16,
                watch: true
            };
            _this.map.on("locationfound", _this.locationFoundCallback);
            _this.map = _this.map.locate(options);
            //Add center control
            var centerMapControl = leaflet__WEBPACK_IMPORTED_MODULE_1__["Control"].extend({
                options: {
                    position: 'bottomright'
                    //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                },
                onAdd: function (map) {
                    var container = leaflet__WEBPACK_IMPORTED_MODULE_1__["DomUtil"].create('button', 'material-icons md-24 leaflet-bar leaflet-control leaflet-control-custom');
                    container.style.width = '30px';
                    container.style.height = '30px';
                    container.innerText = "my_location";
                    container.style.backgroundColor = 'white';
                    container.style.display = "inline block;";
                    container.style.padding = "0px";
                    container.style.cursor = "pointer";
                    container.onclick = function (event) { _this.centerMap(container); };
                    return container;
                },
            });
            var polygonUploadControl = leaflet__WEBPACK_IMPORTED_MODULE_1__["Control"].extend({
                options: {
                    position: 'bottomleft'
                    //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                },
                onAdd: function (map) {
                    var container = leaflet__WEBPACK_IMPORTED_MODULE_1__["DomUtil"].create('button', 'material-icons md-24 leaflet-bar leaflet-control leaflet-control-custom');
                    container.style.width = '30px';
                    container.style.height = '30px';
                    container.innerText = "cloud_upload";
                    container.style.backgroundColor = 'white';
                    container.style.display = "inline block;";
                    container.style.padding = "0px";
                    container.style.cursor = "pointer";
                    container.onclick = function (event) {
                        container.blur();
                        _this.dialog.open(_lab4_polygonInput_component_lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_5__["Lab4PolygonInputComponent"], {
                            "width": "350px",
                            "height": "350px"
                        });
                    };
                    return container;
                },
            });
            _this.map.addControl(new centerMapControl());
            _this.map.addControl(new polygonUploadControl());
        };
        this.onSubmit = function (container) {
        };
        this.centerMap = function (container) {
            container.blur();
            if (_this.map != undefined && _this.userMarker != undefined) {
                _this.map.invalidateSize();
                _this.map.setView(_this.userMarker.getLatLng(), 16);
            }
        };
        this.locationFoundCallback = function (e) { return _this.drawUserMarker(e.latlng); };
        this.drawUserMarker = function (latlng) {
            if (_this.userMarker != undefined) {
                _this.userMarker.removeFrom(_this.map);
            }
            var marker = leaflet__WEBPACK_IMPORTED_MODULE_1__["marker"](latlng);
            marker.setIcon(leaflet__WEBPACK_IMPORTED_MODULE_1__["icon"]({
                iconSize: [32, 32],
                iconAnchor: [13, 41],
                iconUrl: 'assets/position-marker.png'
            }));
            marker.addTo(_this.map);
            if (!_this.alreadySet) {
                _this.map.setView(latlng, 16);
                _this.map.invalidateSize();
                _this.alreadySet = true;
            }
            _this.userMarker = marker;
        };
        this.polygonDrawedCallback = function (e) {
            var type = e.layerType, layer = e.layer;
            if (_this.lastLayer != undefined)
                _this.lastLayer.removeFrom(_this.map);
            if (_this.lastMarkers != undefined)
                _this.lastMarkers.forEach(function (marker) { return marker.removeFrom(_this.map); });
            _this.lastMarkers = [];
            layer.addTo(_this.map);
            _this.lastLayer = layer;
            _this.positionSearchService.searchPositions(layer.toGeoJSON());
        };
    }
    Lab4MapComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-map',
            template: __webpack_require__(/*! ./lab4.map.component.html */ "./src/app/lab4.map.component/lab4.map.component.html"),
            styles: [__webpack_require__(/*! ./lab4.map.component.scss */ "./src/app/lab4.map.component/lab4.map.component.scss")]
        }),
        __metadata("design:paramtypes", [_lab4_positionSearch_component_lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_4__["Lab4PositionSearchService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], Lab4MapComponent);
    return Lab4MapComponent;
}());



/***/ }),

/***/ "./src/app/lab4.map.component/lab4.map.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/lab4.map.component/lab4.map.module.ts ***!
  \*******************************************************/
/*! exports provided: Lab4MapModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4MapModule", function() { return Lab4MapModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lab4_map_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lab4.map.component */ "./src/app/lab4.map.component/lab4.map.component.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _asymmetrik_ngx_leaflet_draw__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet-draw */ "./node_modules/@asymmetrik/ngx-leaflet-draw/dist/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _lab4_polygonInput_component_lab4_polygonInput_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../lab4.polygonInput.component/lab4.polygonInput.module */ "./src/app/lab4.polygonInput.component/lab4.polygonInput.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var Lab4MapModule = /** @class */ (function () {
    function Lab4MapModule() {
    }
    Lab4MapModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _lab4_map_component__WEBPACK_IMPORTED_MODULE_1__["Lab4MapComponent"]
            ],
            imports: [
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_2__["LeafletModule"].forRoot(),
                _asymmetrik_ngx_leaflet_draw__WEBPACK_IMPORTED_MODULE_3__["LeafletDrawModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _lab4_polygonInput_component_lab4_polygonInput_module__WEBPACK_IMPORTED_MODULE_5__["Lab4PolygonInputModule"]
            ],
            providers: [],
            entryComponents: [],
            exports: [_lab4_map_component__WEBPACK_IMPORTED_MODULE_1__["Lab4MapComponent"]]
        })
    ], Lab4MapModule);
    return Lab4MapModule;
}());



/***/ }),

/***/ "./src/app/lab4.notfound.component/lab4.notfound.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/lab4.notfound.component/lab4.notfound.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-center\">\n    <h1>404 Not Found</h1>\n    <p>You may be lost. Follow the breadcrumbs back <a routerLinkActive=\"active\" routerLink=\"/\">home</a>.</p>\n</div>"

/***/ }),

/***/ "./src/app/lab4.notfound.component/lab4.notfound.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/lab4.notfound.component/lab4.notfound.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/lab4.notfound.component/lab4.notfound.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/lab4.notfound.component/lab4.notfound.component.ts ***!
  \********************************************************************/
/*! exports provided: Lab4NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4NotFoundComponent", function() { return Lab4NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Lab4NotFoundComponent = /** @class */ (function () {
    function Lab4NotFoundComponent() {
    }
    Lab4NotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "lab4-notfound",
            template: __webpack_require__(/*! ./lab4.notfound.component.html */ "./src/app/lab4.notfound.component/lab4.notfound.component.html"),
            styles: [__webpack_require__(/*! ./lab4.notfound.component.scss */ "./src/app/lab4.notfound.component/lab4.notfound.component.scss")]
        })
    ], Lab4NotFoundComponent);
    return Lab4NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/lab4.notfound.component/lab4.notfound.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/lab4.notfound.component/lab4.notfound.module.ts ***!
  \*****************************************************************/
/*! exports provided: Lab4NotFoundModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4NotFoundModule", function() { return Lab4NotFoundModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _lab4_notfound_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lab4.notfound.component */ "./src/app/lab4.notfound.component/lab4.notfound.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Lab4NotFoundModule = /** @class */ (function () {
    function Lab4NotFoundModule() {
    }
    Lab4NotFoundModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_lab4_notfound_component__WEBPACK_IMPORTED_MODULE_2__["Lab4NotFoundComponent"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            exports: [_lab4_notfound_component__WEBPACK_IMPORTED_MODULE_2__["Lab4NotFoundComponent"]]
        })
    ], Lab4NotFoundModule);
    return Lab4NotFoundModule;
}());



/***/ }),

/***/ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/lab4.polygonInput.component/lab4.polygonInput.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    <mat-form-field>\n        <textarea matInput rows=\"11\" placeholder=\"Polygon\"></textarea>\n    </mat-form-field>\n    <mat-card-actions>\n        <button color=\"primary\" style=\"margin-left: 17px\" (click)=draw() mat-raised-button>Draw</button>\n    </mat-card-actions>\n"

/***/ }),

/***/ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/lab4.polygonInput.component/lab4.polygonInput.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field {\n  width: 100%; }\n\ntextarea {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/lab4.polygonInput.component/lab4.polygonInput.component.ts ***!
  \****************************************************************************/
/*! exports provided: Lab4PolygonInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4PolygonInputComponent", function() { return Lab4PolygonInputComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Lab4PolygonInputComponent = /** @class */ (function () {
    function Lab4PolygonInputComponent() {
        this.headerText = "Insert a polygon";
    }
    Lab4PolygonInputComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-polygonInput',
            template: __webpack_require__(/*! ./lab4.polygonInput.component.html */ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.html"),
            styles: [__webpack_require__(/*! ./lab4.polygonInput.component.scss */ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.scss")]
        })
    ], Lab4PolygonInputComponent);
    return Lab4PolygonInputComponent;
}());



/***/ }),

/***/ "./src/app/lab4.polygonInput.component/lab4.polygonInput.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/lab4.polygonInput.component/lab4.polygonInput.module.ts ***!
  \*************************************************************************/
/*! exports provided: Lab4PolygonInputModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4PolygonInputModule", function() { return Lab4PolygonInputModule; });
/* harmony import */ var _lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lab4.polygonInput.component */ "./src/app/lab4.polygonInput.component/lab4.polygonInput.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var Lab4PolygonInputModule = /** @class */ (function () {
    function Lab4PolygonInputModule() {
    }
    Lab4PolygonInputModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_0__["Lab4PolygonInputComponent"]
            ],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]
            ],
            providers: [],
            entryComponents: [_lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_0__["Lab4PolygonInputComponent"]],
            exports: [_lab4_polygonInput_component__WEBPACK_IMPORTED_MODULE_0__["Lab4PolygonInputComponent"]]
        })
    ], Lab4PolygonInputModule);
    return Lab4PolygonInputModule;
}());



/***/ }),

/***/ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/lab4.positionSearch.component/lab4.positionSearch.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"lab4-root\">\n\n    <div class=\"lab4-map-container\">\n    <lab4-map></lab4-map>\n    </div>\n    <div class=\"form-root-container\" >\n        <mat-card class=\"form-card\">\n            <mat-card-title-group>\n                <mat-card-title>Filter positions</mat-card-title>\n            </mat-card-title-group>\n            <mat-card-content>\n                <div class=\"datepicker-container\">\n            <mat-form-field class=\"datepicker\" #datepickerFrom>\n                <input matInput [matDatepicker]=\"picker\" \n                [(max)]=\"dateTo\"\n                [(ngModel)]=\"dateFrom\" \n                (ngModelChange)=\"onDateChanged()\"\n                placeholder=\"From\">\n                <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n                <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>\n            <mat-form-field class=\"datepicker\" #datepickerTo>\n                <input matInput [matDatepicker]=\"picker2\" \n                [(min)]=\"dateFrom\" \n                [(ngModel)]=\"dateTo\"\n                (ngModelChange)=\"onDateChanged()\"\n                placeholder=\"To\">\n                <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\n                <mat-datepicker #picker2></mat-datepicker>\n            </mat-form-field>\n            </div>\n            <table mat-table [dataSource]=\"((dataSource))\" class=\"position-list\">\n\n                    <!--- Note that these columns can be defined in any order.\n                          The actual rendered columns are set as a property on the row definition\" -->\n                  \n                    <!-- Position Column -->\n                    <!-- <ng-container matColumnDef=\"position\">\n                      <th mat-header-cell *matHeaderCellDef> PositionId </th>\n                      <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\n                      <td mat-footer-cell *matFooterCellDef> Total</td>\n                    </ng-container> -->\n                  \n                    <!-- Name Column -->\n                    <ng-container matColumnDef=\"longitude\">\n                      <th mat-header-cell *matHeaderCellDef> Longiutde </th>\n                      <td mat-cell *matCellDef=\"let element\"> {{element.geometry.coordinates[1]}} </td>\n                      <td mat-footer-cell *matFooterCellDef> Total</td>\n                    </ng-container>\n                  \n                    <!-- Weight Column -->\n                    <ng-container matColumnDef=\"latitude\">\n                      <th mat-header-cell *matHeaderCellDef> Latitude </th>\n                      <td mat-cell *matCellDef=\"let element\"> {{element.geometry.coordinates[0]}} </td>\n                      <td mat-footer-cell *matFooterCellDef></td>\n                    </ng-container>\n                  \n                    <!-- Symbol Column -->\n                    <ng-container matColumnDef=\"cost\">\n                      <th mat-header-cell *matHeaderCellDef> Cost </th>\n                      <td mat-cell *matCellDef=\"let element\"> {{element.properties.cost}} </td>\n                      <td mat-footer-cell *matFooterCellDef> {{getTotalCost()}}</td>                    </ng-container>\n                  \n                    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                    <tr mat-footer-row *matFooterRowDef=\"displayedColumns\"></tr>\n\n                  </table>\n            <mat-progress-bar *ngIf=\"!progressBarHidden\" class=\"progressBar\" mode=\"indeterminate\"></mat-progress-bar>\n            \n            <mat-paginator \n            #positionsPaginator\n            [length]=((tableRowsSize))\n              [pageSize]=\"5\"\n              [pageSizeOptions]=\"[5, 10, 25, 100]\">\n              \n                                  \n            </mat-paginator>\n\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/lab4.positionSearch.component/lab4.positionSearch.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".lab4-root {\n  display: -webkit-flex;\n  height: 93%;\n  display: flex;\n  flex-flow: row wrap;\n  align-content: center;\n  align-items: center; }\n\n.lab4-map-container {\n  flex: 1;\n  height: 90%;\n  margin: 3% 2%;\n  box-shadow: 0px 10px 24px -4px rgba(0, 0, 0, 0.75);\n  padding: 0; }\n\ntr.mat-footer-row {\n  font-weight: bold; }\n\n.form-root-container {\n  flex: 1;\n  max-width: 500;\n  max-height: 93%;\n  margin: 0 3%; }\n\n.datepicker-container {\n  display: flex;\n  align-content: center;\n  align-items: center; }\n\n.datepicker {\n  flex: 1; }\n\n#datepicker-from {\n  padding-right: 2%; }\n\n.position-list {\n  flex: 1;\n  width: 98%; }\n\n.progressBar {\n  height: 3px; }\n\n/* Medium screens */\n\n@media all and (max-width: 800px) {\n  .lab4-root {\n    /* When on medium sized screens, we center it by evenly distributing empty space around items */\n    flex-flow: column;\n    align-items: stretch; }\n  .lab4-map-container {\n    max-height: 40%; }\n  .form-root-container {\n    max-height: 55%; } }\n"

/***/ }),

/***/ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/lab4.positionSearch.component/lab4.positionSearch.component.ts ***!
  \********************************************************************************/
/*! exports provided: Lab4PositionSearch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4PositionSearch", function() { return Lab4PositionSearch; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lab4.positionSearch.service */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Lab4PositionSearch = /** @class */ (function () {
    function Lab4PositionSearch(positionSearchService, snackbar, cdRef) {
        var _this = this;
        this.positionSearchService = positionSearchService;
        this.snackbar = snackbar;
        this.cdRef = cdRef;
        this.progressBarHidden = true;
        this.dateFrom = new Date();
        this.dateTo = new Date();
        this.loadPage = function () {
            _this.paginator.length = _this.data.length;
            var startIndex = _this.paginator.pageIndex * _this.paginator.pageSize;
            var endIndex = startIndex + _this.paginator.pageSize;
            _this.dataSource = _this.data.slice(startIndex, endIndex < _this.data.length ? endIndex : _this.data.length);
        };
        this.tableRowsSize = 60;
        this.displayedColumns = ['longitude', 'latitude', 'cost'];
        this.dataSource = [];
        this.data = [];
        this.onDateChanged = function (event) {
            if (_this.positionSearchService.searchPositions() == false) {
                _this.progressBarHidden = true;
                _this.snackbar.open("First draw a polygon", "OK", { duration: 2000 });
            }
        };
        this.addPoint = function (point) {
            if (!_this.data.includes(point)) {
                _this.data.push(point);
            }
        };
        this.getTotalCost = function () {
            var total = 0.0;
            _this.data.forEach(function (element) { return total += parseFloat(element.properties.cost); });
            return total;
        };
        this.setupLoading = function () {
            _this.data = [];
            _this.dataSource = [];
            _this.paginator.pageIndex = 0;
            _this.progressBarHidden = false;
            _this.cdRef.detectChanges();
        };
    }
    Lab4PositionSearch.prototype.ngOnInit = function () {
        var _this = this;
        this.dateTo.setDate(this.dateFrom.getDate() + 1);
        this.data.forEach(function (element) { return element.timestamp = _this.dateFrom.valueOf(); });
        this.tableRowsSize = this.data.length;
        this.positionSearchService.setpositionSearchComponent(this);
        this.paginator.pageIndex = 0;
        this.paginator.pageSize = 5;
        this.paginator.page.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function () { return _this.loadPage(); })).subscribe();
        this.loadPage();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("positionsPaginator"),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], Lab4PositionSearch.prototype, "paginator", void 0);
    Lab4PositionSearch = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-pos-search',
            template: __webpack_require__(/*! ./lab4.positionSearch.component.html */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.html"),
            styles: [__webpack_require__(/*! ./lab4.positionSearch.component.scss */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.scss")]
        }),
        __metadata("design:paramtypes", [_lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_3__["Lab4PositionSearchService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], Lab4PositionSearch);
    return Lab4PositionSearch;
}());



/***/ }),

/***/ "./src/app/lab4.positionSearch.component/lab4.positionSearch.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/lab4.positionSearch.component/lab4.positionSearch.module.ts ***!
  \*****************************************************************************/
/*! exports provided: Lab4PositionSearchModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4PositionSearchModule", function() { return Lab4PositionSearchModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lab4_positionSearch_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lab4.positionSearch.component */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.component.ts");
/* harmony import */ var _lab4_map_component_lab4_map_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../lab4.map.component/lab4.map.module */ "./src/app/lab4.map.component/lab4.map.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./lab4.positionSearch.service */ "./src/app/lab4.positionSearch.component/lab4.positionSearch.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var Lab4PositionSearchModule = /** @class */ (function () {
    function Lab4PositionSearchModule() {
    }
    Lab4PositionSearchModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _lab4_positionSearch_component__WEBPACK_IMPORTED_MODULE_1__["Lab4PositionSearch"]
            ],
            imports: [
                _lab4_map_component_lab4_map_module__WEBPACK_IMPORTED_MODULE_2__["Lab4MapModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressBarModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"]
            ],
            providers: [_lab4_positionSearch_service__WEBPACK_IMPORTED_MODULE_5__["Lab4PositionSearchService"]],
            entryComponents: [],
            exports: [_lab4_positionSearch_component__WEBPACK_IMPORTED_MODULE_1__["Lab4PositionSearch"]]
        })
    ], Lab4PositionSearchModule);
    return Lab4PositionSearchModule;
}());



/***/ }),

/***/ "./src/app/lab4.positionSearch.component/lab4.positionSearch.service.ts":
/*!******************************************************************************!*\
  !*** ./src/app/lab4.positionSearch.component/lab4.positionSearch.service.ts ***!
  \******************************************************************************/
/*! exports provided: Lab4PositionSearchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4PositionSearchService", function() { return Lab4PositionSearchService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lab4_serverRequests_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../lab4.serverRequests.service */ "./src/app/lab4.serverRequests.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Lab4PositionSearchService = /** @class */ (function () {
    function Lab4PositionSearchService(serverRequestsService) {
        var _this = this;
        this.serverRequestsService = serverRequestsService;
        this.setpositionSearchComponent = function (component) { return _this.positionSearchComponent = component; };
        this.searchPositions = function (inside) {
            if (inside === void 0) { inside = undefined; }
            var from = _this.positionSearchComponent.dateFrom;
            var to = _this.positionSearchComponent.dateTo;
            _this.positionSearchComponent.setupLoading();
            if (inside == undefined)
                if (_this.lastPolygon == undefined)
                    return false;
                else
                    inside = _this.lastPolygon;
            else
                _this.lastPolygon = inside;
            var positionSearch = _this.serverRequestsService
                .getPositionsByPolygonWithinAndTimestampBetween(inside, from, to)
                .forEach(function (point) { return _this.positionSearchComponent.addPoint(point); })
                .then(function () {
                _this.positionSearchComponent.loadPage();
                _this.positionSearchComponent.progressBarHidden = true;
                _this.positionSearchComponent.cdRef.detectChanges();
            });
            return true;
        };
    }
    Lab4PositionSearchService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_lab4_serverRequests_service__WEBPACK_IMPORTED_MODULE_1__["Lab4ServerRequestsService"]])
    ], Lab4PositionSearchService);
    return Lab4PositionSearchService;
}());



/***/ }),

/***/ "./src/app/lab4.register.component/lab4.register.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/lab4.register.component/lab4.register.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-card class=\"register-card\" fxFlex>\n        <mat-card-title-group>\n            <mat-card-title class=\"register-header\">\n                {{headerText}}\n            </mat-card-title>\n        </mat-card-title-group>\n        <form [formGroup]=\"userRegistrationForm\">\n\n        <mat-card-content class=\"form-content\">\n        <mat-form-field>\n            <mat-icon matPrefix>person</mat-icon>\n            <input matInput placeholder=\"Username\" required>\n        </mat-form-field>\n        <div formGroupName=\"emailGroup\" class=\"form-content\">\n        <mat-form-field>\n            <mat-icon matPrefix>email</mat-icon>\n            <input matInput placeholder=\"Email\" formControlName=\"email\" required>\n            <mat-error>\n                {{errors.email}}\n            </mat-error>\n        </mat-form-field>\n        <mat-form-field>\n            <mat-icon matPrefix>email</mat-icon>\n            <input matInput placeholder=\"Confirm Email\" formControlName=\"confirmEmail\" [errorStateMatcher]=\"confirmValidParentMatcher\" required>\n            <mat-error>\n                {{errors.confirmEmail}}\n            </mat-error>\n        </mat-form-field>\n        </div>\n        <div formGroupName=\"passwordGroup\" class=\"form-content\">\n        <mat-form-field>\n            <mat-icon matPrefix>lock</mat-icon>\n            <input matInput placeholder=\"Password\" type=\"password\" formControlName=\"password\" required>\n            <mat-error>\n                {{errors.password}}\n            </mat-error>\n        </mat-form-field>\n        <mat-form-field>\n            <mat-icon matPrefix>lock</mat-icon>\n            <input matInput placeholder=\"Confirm password\" type=\"password\" formControlName=\"confirmPassword\" [errorStateMatcher]=\"confirmValidParentMatcher\" required>\n            <mat-error>\n                {{errors.confirmPassword}}\n            </mat-error>\n        </mat-form-field>\n        </div>\n        </mat-card-content>\n        <mat-card-actions>\n            <button color=\"primary\" mat-raised-button>Sign up</button>\n        </mat-card-actions>\n    </form>\n</mat-card>"

/***/ }),

/***/ "./src/app/lab4.register.component/lab4.register.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/lab4.register.component/lab4.register.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".register-card {\n  max-width: 500px;\n  margin: 0 auto;\n  margin-top: 15px; }\n\n.form-content {\n  display: flex;\n  flex-direction: column; }\n\nmat-form-field {\n  margin-top: 8px; }\n\nmat-icon {\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/lab4.register.component/lab4.register.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/lab4.register.component/lab4.register.component.ts ***!
  \********************************************************************/
/*! exports provided: Lab4RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4RegisterComponent", function() { return Lab4RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lab4.register.validators */ "./src/app/lab4.register.component/lab4.register.validators.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Lab4RegisterComponent = /** @class */ (function () {
    function Lab4RegisterComponent() {
        var _this = this;
        this.headerText = "Register";
        this.errors = _lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__["errorMessages"];
        this.confirmValidParentMatcher = new _lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__["ConfirmValidParentMatcher"]();
        this.createForm = function () {
            _this.userRegistrationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                emailGroup: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                    confirmEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
                }, { validators: _lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].childrenEqual }),
                passwordGroup: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(_lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__["regExps"].password)]),
                    confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
                }, { validators: _lab4_register_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].childrenEqual })
            });
        };
    }
    Lab4RegisterComponent.prototype.ngOnInit = function () {
        this.createForm();
    };
    Lab4RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-register',
            template: __webpack_require__(/*! ./lab4.register.component.html */ "./src/app/lab4.register.component/lab4.register.component.html"),
            styles: [__webpack_require__(/*! ./lab4.register.component.scss */ "./src/app/lab4.register.component/lab4.register.component.scss")]
        })
    ], Lab4RegisterComponent);
    return Lab4RegisterComponent;
}());



/***/ }),

/***/ "./src/app/lab4.register.component/lab4.register.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/lab4.register.component/lab4.register.module.ts ***!
  \*****************************************************************/
/*! exports provided: Lab4RegisterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4RegisterModule", function() { return Lab4RegisterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _lab4_register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./lab4.register.component */ "./src/app/lab4.register.component/lab4.register.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var Lab4RegisterModule = /** @class */ (function () {
    function Lab4RegisterModule() {
    }
    Lab4RegisterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _lab4_register_component__WEBPACK_IMPORTED_MODULE_4__["Lab4RegisterComponent"]
            ],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            providers: [],
            entryComponents: [],
            exports: [_lab4_register_component__WEBPACK_IMPORTED_MODULE_4__["Lab4RegisterComponent"]]
        })
    ], Lab4RegisterModule);
    return Lab4RegisterModule;
}());



/***/ }),

/***/ "./src/app/lab4.register.component/lab4.register.validators.ts":
/*!*********************************************************************!*\
  !*** ./src/app/lab4.register.component/lab4.register.validators.ts ***!
  \*********************************************************************/
/*! exports provided: patternValidator, regExps, ConfirmValidParentMatcher, CustomValidators, errorMessages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "patternValidator", function() { return patternValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regExps", function() { return regExps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmValidParentMatcher", function() { return ConfirmValidParentMatcher; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomValidators", function() { return CustomValidators; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorMessages", function() { return errorMessages; });
function patternValidator(regexp) {
    return function (control) {
        var value = control.value;
        if (value === '') {
            return null;
        }
        return !regexp.test(value) ? { 'patternInvalid': { regexp: regexp } } : null;
    };
}
var regExps = {
    password: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/
};
var ConfirmValidParentMatcher = /** @class */ (function () {
    function ConfirmValidParentMatcher() {
    }
    ConfirmValidParentMatcher.prototype.isErrorState = function (control, form) {
        return control.parent.invalid && control.touched;
    };
    return ConfirmValidParentMatcher;
}());

var CustomValidators = /** @class */ (function () {
    function CustomValidators() {
    }
    /**
     * Validates that child controls in the form group are equal
     */
    CustomValidators.childrenEqual = function (formGroup) {
        var _a = Object.keys(formGroup.controls || {}), firstControlName = _a[0], otherControlNames = _a.slice(1);
        var isValid = otherControlNames.every(function (controlName) { return formGroup.get(controlName).value === formGroup.get(firstControlName).value; });
        return isValid ? null : { childrenNotEqual: true };
    };
    return CustomValidators;
}());

var errorMessages = {
    fullName: 'Full name must be between 1 and 128 characters',
    email: 'Email must be a valid email address (username@domain.tld)',
    confirmEmail: 'Email addresses must match',
    password: 'Password must be between 7 and 15 characters, and contain at least one number and special character',
    confirmPassword: 'Passwords must match'
};


/***/ }),

/***/ "./src/app/lab4.serverRequests.service.ts":
/*!************************************************!*\
  !*** ./src/app/lab4.serverRequests.service.ts ***!
  \************************************************/
/*! exports provided: Lab4ServerRequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4ServerRequestsService", function() { return Lab4ServerRequestsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Lab4ServerRequestsService = /** @class */ (function () {
    function Lab4ServerRequestsService(http) {
        var _this = this;
        this.http = http;
        this.getPositionsByPolygonWithinAndTimestampBetween = function (polygon, from, to) {
            console.log("boh");
            return _this.http
                .get("assets/positions.json")
                .pipe(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["delay"](1000), rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["flatMap"](function (points) { return points.features; }), rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"](function (point) { return _this.isPointInPolygon(point, polygon); }), rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"](function (point) { return _this.isPointTimestampInDates(point, from, to); }));
        };
        this.checkPointsInPolygon = function (points, polygon) {
            console.log("polygon");
            console.log(polygon);
            var toReturn = [];
            for (; points.features.length > 0;) {
                var pnt = points.features.pop();
                if (_this.isPointInPolygon(pnt, polygon)) {
                    toReturn.push(pnt);
                }
            }
            return toReturn;
        };
        this.isPointInPolygon = function (point, vs) {
            point = point.geometry.coordinates;
            var polygon = vs.geometry.coordinates[0];
            var x = point[1], y = point[0];
            var inside = false;
            for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
                var xi = polygon[i][0], yi = polygon[i][1];
                var xj = polygon[j][0], yj = polygon[j][1];
                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect)
                    inside = !inside;
            }
            return inside;
        };
        this.isPointTimestampInDates = function (point, from, to) {
            return point.properties.timestamp >= from.valueOf() &&
                point.properties.timestamp <= to.valueOf();
        };
    }
    Lab4ServerRequestsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], Lab4ServerRequestsService);
    return Lab4ServerRequestsService;
}());



/***/ }),

/***/ "./src/app/lab4.sidebar.component/lab4.sidebar.component.html":
/*!********************************************************************!*\
  !*** ./src/app/lab4.sidebar.component/lab4.sidebar.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    <mat-toolbar class=\"mat-toolbar mat-toolbar-multiple-rows\" color=\"primary\">  \n            <a mat-button style=\"height: 0; position: absolute;\"> <!-- 'absorbs' the behavior -->\n            </a> \n        <mat-toolbar-row>\n                <button mat-button routerLinkActive=\"active\" \n                    routerLink=\"/home\">Home</button>\n        </mat-toolbar-row>\n\n        <mat-toolbar-row>\n                <a mat-button routerLinkActive=\"active\" \n                    routerLink=\"/login\">Login</a>\n    </mat-toolbar-row>\n    <mat-toolbar-row>\n            <a mat-button routerLinkActive=\"active\" \n                routerLink=\"/register\">Register</a>\n</mat-toolbar-row>\n</mat-toolbar>"

/***/ }),

/***/ "./src/app/lab4.sidebar.component/lab4.sidebar.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/lab4.sidebar.component/lab4.sidebar.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-toolbar {\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/lab4.sidebar.component/lab4.sidebar.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/lab4.sidebar.component/lab4.sidebar.component.ts ***!
  \******************************************************************/
/*! exports provided: Lab4SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4SidebarComponent", function() { return Lab4SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Lab4SidebarComponent = /** @class */ (function () {
    function Lab4SidebarComponent() {
    }
    Lab4SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lab4-sidebar',
            template: __webpack_require__(/*! ./lab4.sidebar.component.html */ "./src/app/lab4.sidebar.component/lab4.sidebar.component.html"),
            styles: [__webpack_require__(/*! ./lab4.sidebar.component.scss */ "./src/app/lab4.sidebar.component/lab4.sidebar.component.scss")]
        })
    ], Lab4SidebarComponent);
    return Lab4SidebarComponent;
}());



/***/ }),

/***/ "./src/app/lab4.sidebar.component/lab4.sidebar.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/lab4.sidebar.component/lab4.sidebar.module.ts ***!
  \***************************************************************/
/*! exports provided: Lab4SidebarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab4SidebarModule", function() { return Lab4SidebarModule; });
/* harmony import */ var _lab4_sidebar_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lab4.sidebar.component */ "./src/app/lab4.sidebar.component/lab4.sidebar.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var Lab4SidebarModule = /** @class */ (function () {
    function Lab4SidebarModule() {
    }
    Lab4SidebarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _lab4_sidebar_component__WEBPACK_IMPORTED_MODULE_0__["Lab4SidebarComponent"]
            ],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"]
            ],
            providers: [],
            entryComponents: [],
            exports: [_lab4_sidebar_component__WEBPACK_IMPORTED_MODULE_0__["Lab4SidebarComponent"]]
        })
    ], Lab4SidebarModule);
    return Lab4SidebarModule;
}());



/***/ }),

/***/ "./src/app/lab5.authenticationService.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/lab5.authenticationService.service.ts ***!
  \*******************************************************/
/*! exports provided: Lab5AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lab5AuthenticationService", function() { return Lab5AuthenticationService; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Lab5AuthenticationService = /** @class */ (function () {
    function Lab5AuthenticationService(http) {
        var _this = this;
        this.http = http;
        this.apiBaseURL = "http://localhost:8080";
        this.login = function (username, password) {
            var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set("grant_type", "password")
                .set("client_id", "app-internet-oauth2-read-client")
                .set("username", username)
                .set("password", password)
                .set("scope", "read");
            var authn = btoa("app-internet-oauth2-read-client:AppInternet_ReadClient");
            console.log(authn);
            console.log(body.toString());
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set("Authorization", "Basic " + authn);
            console.log(headers);
            var loginURL = _this.apiBaseURL + "/oauth/token";
            console.log(loginURL);
            return _this.http.post(loginURL, body, {
                headers: headers
            })
                .subscribe(function (result) { return _this.setSession(result); }, function (error) { return console.log(error); });
        };
        this.setSession = function (result) {
            if (result && result.token) {
                var expiresAt = moment__WEBPACK_IMPORTED_MODULE_0__().add(result.expires_in, "second");
                console.log(jwt_decode__WEBPACK_IMPORTED_MODULE_3__(result.access_token));
                localStorage.setItem("access_token", result.access_token);
                localStorage.setItem("token_type", result.token_type);
            }
        };
        this.logout = function () {
            localStorage.removeItem("access_token");
            localStorage.removeItem("token_type");
        };
    }
    Lab5AuthenticationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], Lab5AuthenticationService);
    return Lab5AuthenticationService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/convez/node_workspace/lab4-angular/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map